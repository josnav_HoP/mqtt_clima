/*
 * MiscFunctions.c
 *
 *  Created on: Feb 27, 2022
 *      Author: josel
 */


#include "MiscFunctions.h"

MiscError_type Str2SignedNum(uint8_t*InputString,int32_t *OutputNum)
{
	uint8_t* AuxPtr = InputString;
	uint8_t Length = strlen(InputString);
	int32_t OutputValue = 0;
	MiscError_type ReturnValue = NO_ERROR_E;
	uint8_t IsNegative = 0;
	uint8_t DecCounter = 1;

	AuxPtr = AuxPtr+Length-1;

	if('-' < *InputString)
	{
		IsNegative = 1;
		Length--;
	}

	if(Length > 0)
	{
		while(Length--)
		{
			if((*AuxPtr<='9') && (*AuxPtr >='0'))
			{
				OutputValue = OutputValue+(DecCounter * (*AuxPtr));
				DecCounter*=10;
				AuxPtr--;
			}
			else
			{
				ReturnValue = ERROR_E;
				break;
			}
		}
		if(IsNegative)
		{
			OutputValue*=-1;
		}
	}
	else
	{
		ReturnValue = ERROR_E;
	}
	return ReturnValue;
}
