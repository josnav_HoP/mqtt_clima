/*
 * MiscFunctions.h
 *
 *  Created on: Feb 27, 2022
 *      Author: josel
 */

#ifndef MISCFUNCTIONS_H_
#define MISCFUNCTIONS_H_

#include "stdint.h"
#include "stdio.h"
#include <string.h>

#endif /* MISCFUNCTIONS_H_ */

typedef enum{
	NO_ERROR_E = 0,
	ERROR_E
}MiscError_type;

MiscError_type Str2SignedNum(uint8_t*InputString,int32_t *OutputNum);
